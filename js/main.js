let canvas = document.getElementById("canvas")
let circles = []
let radius = 30
let context = canvas.getContext("2d")
let color
canvas.width = canvas.clientWidth
canvas.height = canvas.clientHeight

function drawCircle(x, y, radius, border, borderColor, fillColor)
{
    context.beginPath()
    context.arc(x, y, radius, 0, 2*Math.PI)
    context.strokeStyle = borderColor
    context.fillStyle = fillColor
    context.lineWidth = border
    context.closePath()
    context.fill()
    context.stroke()
}

for(let i = 0; i < 20; i++) {
    let x = radius + (Math.random() * (canvas.width - (2 * radius)))
    let y = radius + (Math.random() * (canvas.height - (2 * radius)))
    let color = 'rgb(34, 100, 144)'
    let direction = Math.random() * 2.0 * Math.PI
    circles.push({
        x: x,
        y: y,
        color:color,
        direction:direction
    })
   move();
}

function randomColor() {
    const colors = [
        '34, 100, 144', '242, 57, 66'
    ]
    color = colors[Math.floor(Math.random() * colors.length)]
    return color;
}

function move() {
    context.clearRect(0, 0, canvas.width, canvas.height)
    circles.forEach(function (circle) {
        circle.x = circle.x + Math.cos(circle.direction) / 15
        circle.y = circle.y + Math.sin(circle.direction) / 15
        color = circle.color
        bounce(circle)
        drawCircle(circle.x, circle.y, radius, 5, color, color)
    });
    requestAnimationFrame(move);
}
function bounce(circle) {
    if(((circle.x - radius) < 0) || ((circle.y - radius) < 0) || ((circle.x + radius) > canvas.width) || ((circle.y + radius) > canvas.height))  {
        circle.direction += (Math.PI / 2)
    }
    if(circle.direction > (2 * Math.PI)) circle.direction -= (2 * Math.PI);
}

const promises = {
    good: [
        'шутить смешнее',
        'лечить зубы',
        'распределять свое время',
        'останавливаться и делать что-то для себя',
        'просить прощения у тех, кого обидел',
        'читать по-английски',
        'учить казахский',
        'обнимать почаще того, с кем живу'
    ],
    bad: [
        'чавкать',
        'заниматься самолечением',
        'ругаться в соцсетях',
        'неудачные попытки похудеть',
        'неудачные попытки набрать массу',
        'ходить к плохому психоаналитику',
        'писать похой код',
        'ходить на лекции по средневековью',
        'винить во всем страну'
    ]
}
function givePromise() {
    const good = document.getElementById('goodPromise')
    const bad = document.getElementById('badPromise')
    good.innerText = randPromise().good
    bad.innerText = randPromise().bad
}
function randPromise() {
    let randGood = Math.floor(Math.random() * promises.good.length);
    let randBad = Math.floor(Math.random() * promises.bad.length);

    let promise = {
        good: promises.good[randGood],
        bad: promises.bad[randBad]
    }
    return promise
}